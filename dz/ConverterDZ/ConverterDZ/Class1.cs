﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ConverterDZ
{
    class Converter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            string dateString = "";
            string dS = "";
            DateTime dt = new DateTime();
            foreach (string value in values)
            {
                dS = value;
                dateString += dS;
            }
            switch ((string)parameter)
            {
                case "ddMMyy":
                default:
                    dt = DateTime.ParseExact(dateString, @"ddMMyy", culture);
                    break;
                case "ddMMyyyy":
                    dt = DateTime.ParseExact(dateString, @"ddMMyyyy", culture);
                    break;
                case "MMddyy":
                    dt = DateTime.ParseExact(dateString, @"MMddyy", culture);
                    break;
            }
            return dt;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            string dateString = null;
            DateTime dt = (DateTime)value;
            string[] dates = new string[1];
            switch ((string)parameter)
            {
                case "dd/MM/yyyy":
                default:
                    dateString = dt.ToString(@"dd/MM/yyyy", CultureInfo.InvariantCulture);
                    break;
                case "dd/MM/yy":
                    dateString = dt.ToString(@"dd/MM/yy", CultureInfo.InvariantCulture);
                    break;
                case "MM/dd/yy":
                    dateString = dt.ToString(@"MM/dd/yy", CultureInfo.InvariantCulture);
                    break;
            }
            dates[0] = dateString;
            return dates;
        }

    }

}

