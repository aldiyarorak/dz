﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           
            
        }
        
        public string[] items = new string[] {"Колбаса", "Хлеб", "Молоко"};
        
        public int[] prices = new int[] { 8, 9 , 15 };
        public int totalPrice = 0;
        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.AddRange(items);
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            

            string selectedProduct = comboBox1.SelectedItem.ToString();
            int price = comboBox1.SelectedIndex;
            allItemsTextBox.Text += selectedProduct += "\n";
            priceBox.Text = prices[price].ToString();
            totalPrice += prices[price];
            totalPriceBox.Text = totalPrice.ToString();
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
            Data.EventHandler(comboBox1.SelectedIndex, comboBox1.Text, Convert.ToInt32(priceBox.Text));
            items[comboBox1.SelectedIndex] = comboBox1.Text;
            prices[comboBox1.SelectedIndex] = Convert.ToInt32(priceBox.Text);
        }
    }
}
