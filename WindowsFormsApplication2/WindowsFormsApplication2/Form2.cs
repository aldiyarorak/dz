﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        void func(string param)
        {
            Form1 f1 = new Form1();
            f1.Show();
        }

        public string[] items = new string[] { "Колбаса", "Хлеб", "Молоко" };
        public string[] itemsDescribing = new string[] { "Колбаса докторская, вкусная", "Хлеб кирпич", "Молоко необыкновенно вкусное" };
        public int[] prices = new int[] { 8, 9, 15 };
        string selectedProduct = "";
        int priceItem = 0;
        private void Form2_Load(object sender, EventArgs e)
        {
            comboBox1.Items.AddRange(items);
            string selectedProduct = comboBox1.SelectedItem.ToString();
            int price = comboBox1.SelectedIndex;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedProduct = comboBox1.SelectedItem.ToString();
            priceItem = comboBox1.SelectedIndex;
            richTextBox1.Text = itemsDescribing[priceItem];
            textBox1.Text = prices[priceItem].ToString();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            items[priceItem] = textBox1.Text;
            itemsDescribing[priceItem]=richTextBox1.Text;
            prices[priceItem] = Convert.ToInt32(textBox1.Text);
            this.Close();
        }
        

    }
}
