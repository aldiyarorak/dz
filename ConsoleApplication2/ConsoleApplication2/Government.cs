﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Government
    {
        private static Government instance;
 
        public string Name { get; private set; }
        private static object flag = new Object();
 
        public Government(string name)
        {
            this.Name = name;
        }
 
    public static Government getInstance(string name)
    {
        if (instance == null)
        {
            lock (flag)
            {
                if (instance == null)
                    instance = new Government(name);
            }
        }
        return instance;
    }
    }
}
